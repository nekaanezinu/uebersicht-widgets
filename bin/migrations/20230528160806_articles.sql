-- Add migration script here
CREATE TABLE IF NOT EXISTS articles (
  id INTEGER PRIMARY KEY NOT NULL,
  title VARCHAR(250) NOT NULL,
  url VARCHAR(250),
  source VARCHAR(250),
  created_at TIMESTAMP NOT NULL
);