const Item = (props) => {
  const { article } = props.article;

  return (
    <div className="hackernews-item">
      {
        <div className="item-title">{article.title}</div>
      }
    </div>
  );
};
  
export default Item;