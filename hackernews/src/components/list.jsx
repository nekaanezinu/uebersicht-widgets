import Item from "./item.jsx";

const List = (props) => {
  const { articles } = props.articles;

  return (
    <div className="hackernews-list">
      {console.log(articles)}
      {articles?.map((article) => (
        <Item key={article.id} article={article} />
      ))}
    </div>
  );
};

export default List;
