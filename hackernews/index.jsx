import moment from "moment/moment";
import { run } from "uebersicht";

export const className = `
  top: 0;
  left: 0;
  box-sizing: border-box;
  margin: auto;
  padding: 10px;
  color: black;
  font-family: Arial Rounded MT Bold;
  font-size: 0.8em;
  background: #4f4f4f75;
  max-width: 30%;

  .list {
    .item {
      display: flex;
      padding: 5px 10px;
      gap: 10px;
      align-items: center;
      background: #ffffff75;
      border-radius: 5px;
      margin: 5px;
      text-decoration: none;
      color: black;

      .article {
        width: 100%;

        .title {
          cursor: pointer;
          font-size: 15px;
        }
  
        .subtext {
          display: flex;
          justify-content: space-between;
          width: 100%;
          align-items: center;

          .score {
            display: flex;
            align-items: center;
            gap: 2px;
            font-size: 12px;
    
            .score-icon {
              span {
                font-size: 13px;
                line-height: 1.2;
              }
            }
          }

          .timestamp {
            background: #ffffff87;
            margin-left: 20px;
            padding: 1px 5px;
            border-radius: 5px;
            font-size: 10px;
          }
        }
      }
    }
  }

  .nav {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 10px;

    .top-articles-nav {
      .top-articles-buttons {
        display: flex;
        justify-content: space-between;
        align-items: center;
        gap: 10px;

        .top-articles-button {
          padding: 5px;
          background: rgba(255, 255, 255, 0.46);
          border-radius: 5px;
          cursor: pointer;

          span {
            font-size: 12px;
            font-weight: bold;
          }
        }

        .top-articles-button.active {
          background: #fff;
        }

        .top-articles-button:hover {
          background: rgba(255, 255, 255, 0.6);
        }
      }
    }

    .nav-refresh {
      padding: 2px;
      background: rgba(255, 255, 255, 0.46);
      border-radius: 5px;
      cursor: pointer; 
      position: absolute;
      bottom: 10px;
      right: 15px;

      .nav-refresh-icon {
        span {
          font-size: 18px;
        }
      }
    }

    .nav-refresh:hover {
      background: rgba(255, 255, 255, 0.6);
    }

    .nav-refresh.loading {
      .nav-refresh-icon {
        span {
          animation: rotate 2s linear infinite;
        }
      }
    }
  }

  @keyframes rotate {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;

export const render = ({ articles, listState, fetchState }, dispatch) => {
  const View = () => (
    <div className="newsstand">
      <List />
      <Nav />
    </div>
  );

  const List = () => (
    <div className="list">
      {articles?.map((article, index) => (
        <Item key={article.id} article={article} index={index} />
      ))}
    </div>
  );

  const Item = (data) => (
    <a className="item" href={data.article.url}>
      <div className="article">
        <div className="title">{data.article.title}</div>
        <div className="subtext">
          <div className="score">
            <div className="score-icon">
              <span className="material-icons">arrow_upward</span>
            </div>
            <div className="score-value">{data.article.score}</div>
          </div>
          <div className="timestamp">{timeAgo(data.article.published_at)}</div>
        </div>
      </div>
    </a>
  );

  const Nav = () => (
    <div className="nav">
      <div className="top-articles-nav">
        <div className="top-articles-buttons">
          <div
            className={topArticlesBtnClassName(listState, "day")}
            onClick={() => {
              dayArticles(dispatch).catch(() => {});
            }}
          >
            Day
          </div>
          <div
            className={topArticlesBtnClassName(listState, "week")}
            onClick={() => {
              weekArticles(dispatch).catch(() => {});
            }}
          >
            Week
          </div>
          <div
            className={topArticlesBtnClassName(listState, "month")}
            onClick={() => {
              monthArticles(dispatch).catch(() => {});
            }}
          >
            Month
          </div>
          <div
            className="top-articles-button"
            onClick={() => {
              resetArticles(dispatch).catch(() => {});
            }}
          >
            <span className="material-icons">close</span>
          </div>
        </div>
      </div>

      <div
        className={navRefreshClassName(fetchState)}
        onClick={() => {
          dispatch({ fetchState: "loading" });
          refreshArticles(dispatch).catch(() => {});
        }}
      >
        <div className="nav-refresh-icon">
          <span className="material-icons">refresh</span>
        </div>
      </div>
    </div>
  );

  return <View />;
};

const topArticlesBtnClassName = (listState, topType) => {
  if (listState == topType) {
    return "top-articles-button active";
  } else {
    return "top-articles-button";
  }
};

const navRefreshClassName = (fetchState) => {
  if (fetchState == "done") {
    return "nav-refresh";
  } else {
    return "nav-refresh loading";
  }
};

const timeAgo = (datetime) => {
  let timeNow = moment();
  let timeUtc = moment.utc(datetime);

  let timeDiff = moment.duration(timeNow.diff(timeUtc));
  let minutes = timeDiff.asMinutes();
  let hours = Math.floor(minutes / 60);

  return `${hours}h ${Math.floor(
    hours > 0 ? minutes - hours * 60 : minutes
  )}m ago`;
};

const dayArticles = async (dispatch) => {
  navArticles(dispatch, "skip", "day", 1).catch(() => {});
};

const weekArticles = async (dispatch) => {
  navArticles(dispatch, "skip", "week", 1).catch(() => {});
};

const monthArticles = async (dispatch) => {
  navArticles(dispatch, "skip", "month", 1).catch(() => {});
};

const refreshArticles = async (dispatch) => {
  navArticles(dispatch, "full", "none", 1).catch(() => {});
};

const navArticles = async (dispatch, updateMode, topQueryMode, page) => {
  const args = [updateMode, topQueryMode, page].join(" ");
  console.log(`./hackernews/scripts/runWithArgs.sh ${args}`);
  run(`./hackernews/scripts/runWithArgs.sh ${args}`).then((data) => {
    dispatch({
      articles: parseArticles(data),
      listState: topQueryMode,
      fetchState: "done",
    });
  });
};

const parseArticles = (data) => {
  const doublEescapeWtf = JSON.parse(data);
  return JSON.parse(doublEescapeWtf);
};

const resetArticles = async (dispatch) => {
  run(`./hackernews/scripts/runOnce.sh`).then((data) => {
    dispatch({
      articles: parseArticles(data),
      listState: "none",
      fetchState: "done",
    });
  });
};

export const updateState = (data, previousState) => ({
  ...previousState,
  ...data,
});

export const command = async (dispatch) => {
  // skip update on init, fetch results quickly
  run(`./hackernews/scripts/runOnce.sh`).then((data) => {
    dispatch({
      articles: parseArticles(data),
      listState: "none",
      fetchState: "done",
    });
  });

  // run update script right after
  run("./hackernews/scripts/runEveryTenMinutes.sh").then((data) => {
    dispatch({
      articles: parseArticles(data),
      listState: "none",
      fetchState: "done",
    });
  });

  execute(() => {
    run("./hackernews/scripts/runEveryTenMinutes.sh").then((data) => {
      dispatch({
        articles: parseArticles(data),
        listState: "none",
        fetchState: "done",
      });
    });
  }, 1000 * 60);
};

export const refreshFrequency = false;
